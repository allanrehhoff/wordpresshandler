## DESCRIPTION: ##
Easily rewrite custom handler urls to a theme template file.

Route /handler/YOUR-FILE-NAME urls to a custom theme file.

This is useful for AJAX and post calls around the default wordpress API

## USAGE: ##
* Create a folder named handlers/ in your theme directory
* In that folder create your handler files.
* URI: /handler/something will map to file something.php in your handler directory
* Use get_header(); and get_footer(); if you want to create a static post-less page.