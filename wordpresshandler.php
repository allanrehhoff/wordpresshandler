<?php
/**
* Plugin Name: ATR Wordpress Handler
* Description: Route custom URI's to a template file
* Version: 1.5
* Author: Allan Thue Rehhoff
* Author URI: http://rehhoff.me
*/
defined( 'ABSPATH' ) or die( "No script kiddies please." );

include "WordpressHandler.class.php";

$wp_handler = new WordpressHandler;

register_activation_hook( __FILE__, array($wp_handler, 'handler_rewrite_endpoint' ));

if($wp_handler->arg(0) == 'handler') {
	add_action( 'init', array($wp_handler, 'handler_template_include'));
}
