<?php
class WordpressHandler {
	protected $args;

	public function __construct() {
		$this->args = explode( "/", $_SERVER['REQUEST_URI'] );
		array_shift( $this->args );
	}

	public function arg( $index = null ) {
		if( $index !== null ) {
			return isset( $this->args[$index] ) ? $this->args[$index] : false;
		} else {
			return $this->args;
		}
	}

	public function trigger_404() {
		status_header(404);
		include  get_404_template();
	}

	public function handler_locate_template( $original_template ) {
		$filename = 'handlers/'.$this->arg(1).'.php';
		$template_path = locate_template( $filename );
		
		if( $template_path != '' ) {
			status_header(200);
			return $template_path;
		} else {
			$this->trigger_404();
		}
	}

	public function handler_template_include() {
		if( $this->arg(1) ) {
			add_filter( 'template_include', array( $this, 'handler_locate_template' ) );
		} else {
			$this->trigger_404();
		}
	}

	//We don't actually have a real use for this one, 
	//and yet we have to do it to avoid getting 404'ed.
	function handler_rewrite_endpoint(){
	    global $wp_rewrite;
	    add_rewrite_endpoint( 'handler', EP_PERMALINK );
	    $wp_rewrite->flush_rules();
	}
}