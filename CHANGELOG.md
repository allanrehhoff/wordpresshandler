# WP Handler changelog #

## v1.5.2 2015.06.19 ##
* Renamed classes and files to match repository name
* Now including themes 404 page, instead of my silly one.

## v1.5 - 2015.02.17 ##
* Properly fixed correct located templates still resulting in 404 errors
* Rewrote code base to objects.
* A few performance optimizations

## v1.0 - 2014.12.16 ##
* Initial release.